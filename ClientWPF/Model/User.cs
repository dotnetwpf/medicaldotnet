﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientWPF.Model
{
    public class User
    {
        private string _login;
        private string _firstName;
        private string _name;
        private string _role;
        private bool _connected;
        private byte[] _picture;

        private string _password;
        private string _confirmPassword;

        public string Login { get { return _login; } set { _login = value; } }
        public string FirstName { get { return _firstName; } set { _firstName = value; } }
        public string Name { get { return _name; } set { _name = value; } }
        public string Role { get { return _role; } set { _role = value; } }
        public bool Connected { get { return _connected; } set { _connected = value; } }
        public byte[] Picture { get { return _picture; } set { _picture = value; } }

        public string Password { get { return _password; } set { _password = value; } }
        public string ConfirmPassword { get { return _confirmPassword; } set { _confirmPassword = value; } }
    }
}
