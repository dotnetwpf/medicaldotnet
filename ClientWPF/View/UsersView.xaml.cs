﻿using System.Windows;
using System.Windows.Controls;

namespace ClientWPF.View
{
    /// <summary>
    /// Description for UsersView.
    /// </summary>
    public partial class UsersView : Page
    {
        /// <summary>
        /// Initializes a new instance of the UsersView class.
        /// </summary>
        public UsersView()
        {
            InitializeComponent();
        }
    }
}