﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace ClientWPF.View
{
    /// <summary>
    /// Description for FullCarouselControl.
    /// </summary>
    public partial class FullCarouselControl : UserControl
    {
        public int SelectedIndex = 0;

        static DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<byte[]>), typeof(FullCarouselControl), new FrameworkPropertyMetadata(new ObservableCollection<byte[]>()));
        public ObservableCollection<byte[]> ItemsSource
        {
            get { return (ObservableCollection<byte[]>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        /// <summary>
        /// Initializes a new instance of the FullCarouselControl class.
        /// </summary>
        public FullCarouselControl()
        {
            InitializeComponent();
        }

        private void CarouselControl_OnElementSelected(object sender)
        {
            SelectedIndex = Carousel.IndexCurrentlySelected;
            ListView.SelectedIndex = SelectedIndex;
            ListView.ScrollIntoView(ListView.SelectedItem);
        }

        private void ListView_Selected(object sender, RoutedEventArgs e)
        {
            SelectedIndex = ListView.SelectedIndex;
            Carousel.SelectElement(SelectedIndex);
        }
    }
}