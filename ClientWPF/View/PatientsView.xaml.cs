﻿using System.Windows;
using System.Windows.Controls;

namespace ClientWPF.View
{
    /// <summary>
    /// Description for PatientsView.
    /// </summary>
    public partial class PatientsView : Page
    {
        /// <summary>
        /// Initializes a new instance of the PatientsView class.
        /// </summary>
        public PatientsView()
        {
            InitializeComponent();
        }
    }
}