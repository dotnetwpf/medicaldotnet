﻿using System.Windows;
using System.Windows.Controls;

namespace ClientWPF.View
{
    /// <summary>
    /// Description for ObservationView.
    /// </summary>
    public partial class ObservationView : Page
    {
        /// <summary>
        /// Initializes a new instance of the ObservationView class.
        /// </summary>
        public ObservationView()
        {
            InitializeComponent();
        }
    }
}