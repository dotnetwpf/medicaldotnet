﻿using System.Windows;
using System.Windows.Controls;

namespace ClientWPF.View
{
    /// <summary>
    /// Description for PatientDetailsView.
    /// </summary>
    public partial class PatientDetailsView : Page
    {
        /// <summary>
        /// Initializes a new instance of the PatientDetailsView class.
        /// </summary>
        public PatientDetailsView()
        {
            InitializeComponent();
        }
    }
}