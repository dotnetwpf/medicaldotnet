﻿using System.Windows;

namespace ClientWPF.View
{
    /// <summary>
    /// Description for HomeView.
    /// </summary>
    public partial class HomeView : Window
    {
        /// <summary>
        /// Initializes a new instance of the HomeView class.
        /// </summary>
        public HomeView()
        {
            InitializeComponent();
        }
    }
}