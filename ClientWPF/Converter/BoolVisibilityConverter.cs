﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ClientWPF.Converter
{
    class BoolVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
            {
                bool invert = false;
                String param = null;
                try
                {
                    param = parameter as String;
                    invert = param.ToLower() == "invert";
                }
                catch (Exception) { }
                if (((bool)value && !invert) || (!(bool)value && invert))
                    return Visibility.Visible;
                else if (param != null && param.ToLower() == "hidden")
                    return Visibility.Hidden;
                else
                    return Visibility.Collapsed;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
