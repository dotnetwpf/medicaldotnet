﻿using ClientWPF.View;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.ComponentModel;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace ClientWPF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class LoginViewModel : ViewModelBase, INotifyPropertyChanged
    {
        private bool _closeSignal = false;
        public bool CloseSignal
        {
            get { return _closeSignal; }
            set {
                if (_closeSignal != value)
                {
                    _closeSignal = value;
                    RaisePropertyChanged("CloseSignal");
                }
            }
        }

        private bool _capsLockDown = Console.CapsLock;
        public bool CapsLockDown
        {
            get { return _capsLockDown; }
            set
            {
                if (_capsLockDown != value)
                {
                    _capsLockDown = value;
                    RaisePropertyChanged("CapsLockDown");
                }
            }
        }

        public bool DisplayErrorMessage { get { return _displayNoCredentialsInfo || _displayWrongCredentialsInfo || _displayExceptionOccured; } }

        private bool _displayNoCredentialsInfo = false;
        public bool DisplayNoCredentialsInfo
        {
            get { return _displayNoCredentialsInfo; }
            set
            {
                if (_displayNoCredentialsInfo != value)
                {
                    _displayNoCredentialsInfo = value;
                    RaisePropertyChanged("DisplayNoCredentialsInfo");
                    RaisePropertyChanged("DisplayErrorMessage");
                }
            }
        }

        private bool _displayWrongCredentialsInfo = false;
        public bool DisplayWrongCredentialsInfo
        {
            get { return _displayWrongCredentialsInfo; }
            set
            {
                if (_displayWrongCredentialsInfo != value)
                {
                    _displayWrongCredentialsInfo = value;
                    RaisePropertyChanged("DisplayWrongCredentialsInfo");
                    RaisePropertyChanged("DisplayErrorMessage");
                }
            }
        }

        private bool _displayExceptionOccured = false;
        public bool DisplayExceptionOccured
        {
            get { return _displayExceptionOccured; }
            set
            {
                if (_displayExceptionOccured != value)
                {
                    _displayExceptionOccured = value;
                    RaisePropertyChanged("DisplayExceptionOccured");
                    RaisePropertyChanged("DisplayErrorMessage");
                }
            }
        }

        private string _capsLockInfo = "Caps Lock is active";
        public string CapsLockInfo { get { return _capsLockInfo; } }

        private string _noCredentialsInfo = "Enter your credentials and try again";
        public string NoCredentialsInfo { get { return _noCredentialsInfo; } }

        private string _wrongCredentialsInfo = "Wrong username and/or password";
        public string WrongCredentialsInfo { get { return _wrongCredentialsInfo; } }

        private string _exceptionOccured = "An error occured. Please restart the application";
        public string ExceptionOccured { get { return _exceptionOccured; } }

        private string _username;
        public string Username { get { return _username; } set { _username = value; } }

        private ICommand _loginCommand;
        public ICommand LoginCommand { get { return _loginCommand; } private set { _loginCommand = value; } }

        private ICommand _updateCapsLockCommand;
        public ICommand UpdateCapsLockCommand { get { return _updateCapsLockCommand; } private set { _updateCapsLockCommand = value; } }

        private ICommand _hideErrorMessageCommand;
        public ICommand HideErrorMessageCommand { get { return _hideErrorMessageCommand; } private set { _hideErrorMessageCommand = value;  } }

        /// <summary>
        /// Initializes a new instance of the LoginViewModel class.
        /// </summary>
        public LoginViewModel()
        {          
            _loginCommand = new RelayCommand<PasswordBox>((param) => LoginAccess(Username, param.Password));
            _updateCapsLockCommand = new RelayCommand(() => { UpdateCapsLock(); });
            _hideErrorMessageCommand = new RelayCommand(() => { HideErrorMessage(); });
        }

        private async void LoginAccess(string username, string password)
        {
            if (username != null && username != "" && password != null && password != "")
            {
                ServiceUser.IServiceUser serviceUser = new ServiceUser.ServiceUserClient();
                try
                {
                    if (serviceUser.Connect(username, password))
                    {
                        Application.Current.Properties.Add("User", await serviceUser.GetUserAsync(username));
                        HomeView home = new HomeView();
                        home.Show();
                        CloseSignal = true;
                    }
                    else
                        DisplayWrongCredentialsInfo = true;
                }
                catch (Exception ex)
                {
                    DisplayExceptionOccured = true;
                }
            }
            else
            {
                DisplayNoCredentialsInfo = true;
            }
        }

        private void UpdateCapsLock()
        {
            CapsLockDown = Console.CapsLock;
        }
        
        private void HideErrorMessage()
        {
            DisplayNoCredentialsInfo = false;
            DisplayWrongCredentialsInfo = false;
            DisplayExceptionOccured = false;
        }

    }
}