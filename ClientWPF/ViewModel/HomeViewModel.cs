﻿using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Controls;

namespace ClientWPF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class HomeViewModel : ViewModelBase
    {
        private ViewModelBase _currentViewModel;

        public ViewModelBase CurrentViewModel
        {
            get { return _currentViewModel; }
            set
            {
                if (_currentViewModel != value)
                {
                    _currentViewModel = value;
                    RaisePropertyChanged("CurrentViewModel");
                    RaisePropertyChanged("CurrentView");
                }
            }
        }

        public string CurrentView
        {
            get
            {
                if (CurrentViewModel is PatientsViewModel)
                    return "PatientsView.xaml";
                else if (CurrentViewModel is PatientDetailsViewModel)
                    return "PatientDetailsView.xaml";
                else if (CurrentViewModel is ObservationViewModel)
                    return "ObservationViewModel.xaml";
                else
                    return "UsersView";
            }
        }

        /// <summary>
        /// Initializes a new instance of the HomeViewModel class.
        /// </summary>
        public HomeViewModel()
        {
            _currentViewModel = ServiceLocator.Current.GetInstance<PatientsViewModel>();
        }
    }
}