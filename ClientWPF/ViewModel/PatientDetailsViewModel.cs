﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;

namespace ClientWPF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class PatientDetailsViewModel : ViewModelBase
    {
        private ServiceObservation.Observation _newObservation;
        public ServiceObservation.Observation NewObservation { get { return _newObservation; } set { if (_newObservation != value) { _newObservation = value;  RaisePropertyChanged("NewObservation"); } } }
        private String prescriptions;

        private Dbo.Patient _patient;
        public Dbo.Patient Patient
        {
            get { return _patient; }
            set
            {
                _patient = value;
                RaisePropertyChanged("Patient");
            }
        }

        private ICollectionView _observations;
        public ICollectionView Observations
        {
            get { return _observations; }
            set
            {
                _observations = value;
                RaisePropertyChanged("Observations");
            }
        }

        public RelayCommand<DateTime> ObservationDetailsCommand { get; private set; }

        private readonly ServicePatient.IServicePatient _patientService;
        private ServiceObservation.IServiceObservation _observationService;

        private ObservableCollection<ServicePatient.Observation> _items;
        public ObservableCollection<ServicePatient.Observation> Items
        {
            get { return _items; }
            private set
            {
                if (_items != value)
                {
                    _items = value;
                    RaisePropertyChanged("Items");
                }
            }
        }

        private ICommand _addCommand;
        public ICommand AddCommand { get { return _addCommand; } private set { if (_addCommand != value) { _addCommand = value; RaisePropertyChanged("AddCommand"); } } }
        private ICommand _displayPopupCommand;
        public ICommand DisplayPopupCommand { get { return _displayPopupCommand; } private set { _displayPopupCommand = value; } }
        private bool _displayPopup = false;
        public bool DisplayPopup { get { return _displayPopup; } private set { if (_displayPopup != value) { _displayPopup = value; RaisePropertyChanged("DisplayPopup"); } } }

        #region NewObservation
        #endregion

        /// <summary>
        /// Initializes a new instance of the PatientDetailsViewModel class.
        /// </summary>
        public PatientDetailsViewModel()
        {
            _patientService = new ServicePatient.ServicePatientClient();
            _observationService = new ServiceObservation.ServiceObservationClient();

            _items = new ObservableCollection<ServicePatient.Observation>();
            AddCommand = new RelayCommand(() => AddObservation());
            NewObservation = new ServiceObservation.Observation();
            DisplayPopupCommand = new RelayCommand(() => DisplayPopup = !DisplayPopup);
            ObservationDetailsCommand = new RelayCommand<DateTime>((d) => ObservationDetailsCommandExecute(d));
        }

        private void InitObservationsCollection(List<Dbo.Observation> observations)
        {
            Observations = CollectionViewSource.GetDefaultView(observations);
        }

        public async void AddObservation()
        {
            NewObservation.Date = DateTime.Now;
            NewObservation.Prescription = prescriptions.Split(',');
            try
            {
                bool result = await _observationService.AddObservationAsync(_patient.Id, NewObservation);
                if (!result)
                {
                    //FIXME
                }
            }
            catch (Exception) { }
            NewObservation = new ServiceObservation.Observation();
            prescriptions = "";
        }

        public async void LoadPatient(int id)
        {
            ServicePatient.Patient servicePatient = await _patientService.GetPatientAsync(id);

            Dbo.Patient p = new Dbo.Patient()
            {
                Id = servicePatient.Id,
                Firstname = servicePatient.Name,
                Name = servicePatient.Name,
                Birthday = servicePatient.Birthday,
                Observations = new List<Dbo.Observation>()
            };

            foreach (var obs in servicePatient.Observations)
            {
                p.Observations.Add(new Dbo.Observation()
                {
                    BloodPressure = obs.BloodPressure,
                    Comment = obs.Comment,
                    Pictures = obs.Pictures,
                    Date = obs.Date,
                    Prescription = obs.Prescription,
                    Weight = obs.Weight
                });
            }

            Patient = p;
            InitObservationsCollection(p.Observations);
        }

        private void ObservationDetailsCommandExecute(DateTime date)
        {
            Dbo.Observation ob = Patient.Observations.Find((o) => o.Date.Equals(date));

            if (ob != null)
            {
                HomeViewModel hvm = ServiceLocator.Current.GetInstance<HomeViewModel>();
                ObservationViewModel ovm = new ObservationViewModel();
                ovm.Load(ob);

                hvm.CurrentViewModel = ovm;
            }
        }
    }
}