﻿using ClientWPF.Model;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace ClientWPF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class UsersViewModel : ViewModelBase
    {
        private ServiceUser.IServiceUser _serviceUser;

        #region Errors
        private bool _displayError;
        public bool DisplayError
        {
            get { return _displayError; }
            private set
            {
                if (_displayError != value)
                {
                    _displayError = value;
                    RaisePropertyChanged("DisplayError");
                }
            }
        }

        private string _error;
        public string Error
        {
            get { return _error; }
            private set
            {
                if (_error != value)
                {
                    _error = value;
                    RaisePropertyChanged("Error");
                }
            }
        }

        private List<User> _failedToDeleteUserList;
        public List<User> FailedToDeleteUserList
        {
            get { return _failedToDeleteUserList; }
            private set
            {
                if (_failedToDeleteUserList != value)
                {
                    _failedToDeleteUserList = value;
                    RaisePropertyChanged("FailedToDeleteUserList");
                }
            }
        }
        #endregion

        #region UserInfo
        private string _userRole;
        public string UserRole
        {
            get { return _userRole; }
            private set
            {
                if (_userRole != value)
                {
                    _userRole = value;
                    RaisePropertyChanged("UserRole");
                }
            }
        }
        #endregion

        /*
        private User _user;
        public User User
        {
            get { return _user; }
            private set
            {
                if (_user != value)
                {
                    _user = value;
                    RaisePropertyChanged("User");
                }
            }
        }
        */

        #region Fields for adding a user
        private bool _displayPopup = false;
        public bool DisplayPopup { get { return _displayPopup; } private set { if (_displayPopup != value) { _displayPopup = value; RaisePropertyChanged("DisplayPopup"); } } }

        private string _firstName;
        public string FirstName { get { return _firstName; } set { if (_firstName != value) { _firstName = value; RaisePropertyChanged("FirstName"); } } }
        private string _lastName;
        public string LastName { get { return _lastName; } set { if (_lastName != value) { _lastName = value; RaisePropertyChanged("LastName"); } } }
        private string _login;
        public string Login { get { return _login; } set { if (_login != value) { _login = value; RaisePropertyChanged("Login"); } } }
        private string _password;
        public string Password { get { return _password; } set { if (_password != value) { _password = value; RaisePropertyChanged("Password"); } } }
        private string _confirmPassword;
        public string ConfirmPassword { get { return _confirmPassword; } set { if (_confirmPassword != value) { _confirmPassword = value; RaisePropertyChanged("ConfirmPassword"); } } }
        private string _role = "Infirmière";
        public string Role { get { return _role; } set { if (_role != value) { _role = value; RaisePropertyChanged("Role"); } } }
        private byte[] _picture;
        public byte[] Picture { get { return _picture; } set { if (_picture != value) { _picture = value; RaisePropertyChanged("Picture"); } } }
        #endregion

        private ObservableCollection<User> _users;
        public ObservableCollection<User> UserList
        {
            get { return _users; }
            private set
            {
                if (_users != value)
                {
                    _users = value;
                    RaisePropertyChanged("UserList");
                }
            }
        }

        private ObservableCollection<User> _selectedItems;
        public ObservableCollection<User> SelectedItems
        {
            get { return _selectedItems; }
            private set
            {
                if (_selectedItems != value)
                {
                    _selectedItems = value;
                    RaisePropertyChanged("SelectedItems");
                }
            }
        }

        #region Command
        private ICommand _deleteCommand;
        public ICommand DeleteCommand { get { return _deleteCommand; } private set { _deleteCommand = value; } }

        private ICommand _addCommand;
        public ICommand AddCommand { get { return _addCommand; } private set { _addCommand = value; } }

        private ICommand _refreshCommand;
        public ICommand RefreshCommand { get { return _refreshCommand; } private set { _refreshCommand = value; } }

        private ICommand _displayPopupCommand;
        public ICommand DisplayPopupCommand { get { return _displayPopupCommand; } private set { _displayPopupCommand = value; } }

        private ICommand _openFileDialogCommand;
        public ICommand OpenFileDialogCommand { get { return _openFileDialogCommand; } private set { _openFileDialogCommand = value; } }
        #endregion

        /// <summary>
        /// Initializes a new instance of the UsersViewModel class.
        /// </summary>
        public UsersViewModel()
        {
            _serviceUser = new ServiceUser.ServiceUserClient();
            UserList = new ObservableCollection<User>();
            SelectedItems = new ObservableCollection<User>();
            ServiceUser.User user = Application.Current.Properties["User"] as ServiceUser.User;
            if (user != null)
                UserRole = user.Role;

#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
            RefreshUsers();
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed

            DeleteCommand = new RelayCommand(() => DeleteUsers());
            AddCommand = new RelayCommand(() => AddUser());
            RefreshCommand = new RelayCommand(() => RefreshUsers());
            DisplayPopupCommand = new RelayCommand(() => DisplayPopup = !DisplayPopup);
            OpenFileDialogCommand = new RelayCommand(() => OpenImageDialog());
        }

        #region CommandTask
        private async void RefreshUsers(int pass = 0)
        {
            try
            {
                ServiceUser.User[] dboUserList = await _serviceUser.GetListUserAsync();
                UserList.Clear();
                foreach (ServiceUser.User dboUser in dboUserList)
                {
                    UserList.Add(new User() { FirstName = dboUser.Firstname, Name = dboUser.Name, Login = dboUser.Login, Role = dboUser.Role, Picture = dboUser.Picture, Connected = dboUser.Connected });
                }
            }
            catch (Exception)
            {
                if (pass < 3)
                    RefreshUsers(++pass);
                else
                {
                    // Display an error
                    Error = "Could not get the list of users. Try again later !";
                    DisplayError = UserList.Count == 0;
                }
            }
        }

        private async void DeleteUsers()
        {
            List<User> failedDelete = new List<User>();
            List<User> successDelete = new List<User>();
            foreach (User user in _selectedItems)
            {
                try
                {
                    bool result = await _serviceUser.DeleteUserAsync(user.Login);
                    if (result)
                        successDelete.Add(user);
                }
                catch (Exception)
                {
                    failedDelete.Add(user);
                }
            }

            foreach (User user in successDelete)
                UserList.Remove(user);
        }

        private async void AddUser(int pass = 0)
        {
            DisplayPopup = false;
            if (Login == "")
            {
                //Login is missing
            }
            else if (FirstName == "")
            {
                //Firstname is missing
            }
            else if (LastName == "")
            {
                //Lastname is missing
            }
            else if (Role == "")
            {
                //Role is missing
            }
            else if (Password == "" || ConfirmPassword == "")
            {
                //Password is missing
            }
            else if (ConfirmPassword != Password)
            {
                //Password and confirm password doesn't match
            }
            else
            {
                ServiceUser.User dboUser = new ServiceUser.User() { Login = Login, Firstname = FirstName, Name = LastName, Pwd = Password, Role = Role, Picture = Picture };
                try
                {
                    bool result = await _serviceUser.AddUserAsync(dboUser);
                    if (!result)
                    {
                        //Fail
                    }
                    else
                    {
                        UserList.Add(new User() { Login = Login, FirstName = FirstName, Name = LastName, Role = Role, Picture = Picture, Connected = false });
                        Login = "";
                        FirstName = "";
                        LastName = "";
                        Role = "Infirmière";
                        Picture = new byte[0];
                    }
                }
                catch (Exception ex)
                {
                    //Fail
                    if (pass < 3)
                        AddUser(++pass);
                    else
                    {
                        //No more call, Print an error

                    }
                }
            }
        }

        private async void OpenImageDialog()
        {
            DisplayPopup = false;
            OpenFileDialog imageDialog = new OpenFileDialog();
            imageDialog.Filter = "Image Files (*.bmp, *.jpg, *.png)|*.bmp;*.jpg;*.jpeg;*.png";
            try
            {
                imageDialog.ShowDialog();
                using (Stream stream = imageDialog.OpenFile())
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        await stream.CopyToAsync(memoryStream);
                        Picture = memoryStream.ToArray();
                    }
                }
            }
            catch (InvalidOperationException) { }
            DisplayPopup = true;

        }
        #endregion
    }
}