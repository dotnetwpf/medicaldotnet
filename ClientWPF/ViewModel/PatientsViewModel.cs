﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Input;

namespace ClientWPF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class PatientsViewModel : ViewModelBase
    {
        private string _searchQuery;
        public string SearchQuery
        {
            get { return _searchQuery; }
            set
            {
                if (_searchQuery != value)
                {
                    _searchQuery = value;
                    RaisePropertyChanged("SearchQuery");
                    Patients.Refresh();
                }
            }
        }

        private readonly ServicePatient.IServicePatient _patientService;

        private ObservableCollection<ServicePatient.Patient> _patientCollection;
        public ObservableCollection<ServicePatient.Patient> PatientCollection { get { return _patientCollection; } private set { if (_patientCollection != value) { _patientCollection = value; RaisePropertyChanged("PatientCollection"); } } }
        
        private ICollectionView _patients;
        public ICollectionView Patients
        {
            get { return _patients; }
            set
            {
                _patients = value;
                RaisePropertyChanged("Patients");
            }
        }

        public RelayCommand<int> PatientDetailsCommand { get; private set; }
        public RelayCommand<ServicePatient.Patient> PatientDeleteCommand { get; private set; }
        public RelayCommand PatientAddCommand { get; private set; }

        private ICommand _displayPopupCommand;
        public ICommand DisplayPopupCommand { get { return _displayPopupCommand; } private set { _displayPopupCommand = value; } }
        private bool _displayPopup = false;
        public bool DisplayPopup { get { return _displayPopup; } private set { if (_displayPopup != value) { _displayPopup = value; RaisePropertyChanged("DisplayPopup"); } } }

        private ServicePatient.Patient _newPatient;
        public ServicePatient.Patient NewPatient { get { return _newPatient; } set { if (_newPatient != value) { _newPatient = value; RaisePropertyChanged("NewPatient"); } } }

        /// <summary>
        /// Initializes a new instance of the PatientViewModel class.
        /// </summary>
        public PatientsViewModel()
        {
            PatientDetailsCommand = new RelayCommand<int>((id) => PatientDetailsCommandExecute(id));
            PatientDeleteCommand = new RelayCommand<ServicePatient.Patient>((patient) => PatientDeleteCommandExecute(patient));
            PatientAddCommand = new RelayCommand(() => AddPatient());
            DisplayPopupCommand = new RelayCommand(() => DisplayPopup = !DisplayPopup);

            _patientService = new ServicePatient.ServicePatientClient();
            InitCollection(_patientService.GetListPatient());
            _newPatient = new ServicePatient.Patient();
        }

        private void InitCollection(ServicePatient.Patient[] patients)
        {
            _patientCollection = new ObservableCollection<ServicePatient.Patient>(patients);
            Patients = CollectionViewSource.GetDefaultView(_patientCollection);
            Patients.Filter += (object item) =>
            {
                if (SearchQuery == null)
                    return true;

                var patient = (ServicePatient.Patient)item;

                return patient.Firstname.Contains(SearchQuery) || patient.Name.Contains(SearchQuery);
            };
        }

        private async void AddPatient()
        {
            DisplayPopup = false;
            bool result = await _patientService.AddPatientAsync(_newPatient);
            if (result)
                _patientCollection.Add(_newPatient);
            _newPatient = new ServicePatient.Patient();
        }

        private void PatientDetailsCommandExecute(int id)
        {
            HomeViewModel hvm = ServiceLocator.Current.GetInstance<HomeViewModel>();
            PatientDetailsViewModel pdvm = new PatientDetailsViewModel();
            pdvm.LoadPatient(id);

            hvm.CurrentViewModel = pdvm;
        }

        private async void PatientDeleteCommandExecute(ServicePatient.Patient patient)
        {
            bool isDeleted = false;

            try
            {
                isDeleted = await _patientService.DeletePatientAsync(patient.Id);

                if (isDeleted)
                {
                    PatientCollection.Remove(patient);
                    Patients.Refresh();
                }
            }
            catch (Exception)
            {
            }
        }
    }
}