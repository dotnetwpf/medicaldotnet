﻿using ClientWPF.ServiceObservation;
using ClientWPF.ServicePatient;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.Windows.Input;

namespace ClientWPF.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ObservationViewModel : ViewModelBase
    {
        private Dbo.Observation _observation;
        public Dbo.Observation Observation { get { return _observation; } set { if (_observation != value) { _observation = value; RaisePropertyChanged("Observation"); } } }

        private IServiceObservation _serviceObservation;

        private ObservableCollection<byte[]> _pictures;
        public ObservableCollection<byte[]> Pictures { get { return _pictures; } set { if (_pictures != value) { _pictures = value; RaisePropertyChanged("Pictures"); } } }

        private ICommand _addCommand;
        public ICommand AddCommand { get { return _addCommand; } set { if (_addCommand != value) { _addCommand = value; RaisePropertyChanged("AddCommand"); } } }

        private byte[] _picture;
        public byte[] Picture { get { return _picture; } set { if (_picture != value) { _picture = value; RaisePropertyChanged("Picture"); } } }

        /// <summary>
        /// Initializes a new instance of the ObservationViewModel class.
        /// </summary>
        public ObservationViewModel()
        {
            _serviceObservation = new ServiceObservation.ServiceObservationClient();
            AddCommand = new RelayCommand(() => AddPicture());
        }

        public void Load(Dbo.Observation observation)
        {
            _observation = observation;
            Pictures = new ObservableCollection<byte[]>(_observation.Pictures);
        }

        public void AddPicture()
        {
            bool result = true;
            //result = _serviceObservation.AddPictureAsync(_observation, Picture);
            if (result)
                Pictures.Add(_picture);

            _picture = null;
        }
    }
}